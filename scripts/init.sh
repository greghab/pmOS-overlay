# so screen script works
chmod 777 -R /sys/devices/platform/omapdrm.0/graphics/fb0/blank

# turn off indicator led
echo 0 > /sys/class/power_supply/bq24150a-0/stat_pin_enable 

# change keybindings for meta keys:

xmodmap -e "keycode 220 = Escape"
xmodmap -e "keycode 124 = Alt_L"
