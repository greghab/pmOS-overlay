#!/bin/sh

seconds=$1;
var=$2;
if [ "$var" = m ];
then
	 seconds=$seconds*60
fi

date1=$((`date +%s` + $seconds)); 
while [ "$date1" -ge `date +%s` ]; do 
	 figlet "$(date -u --date @$(($date1 - `date +%s` )) +%M:%S)";
	 sleep 1
done
