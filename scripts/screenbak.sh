#Power button

#If you want to make use of the power button, bind XF86PowerOff to this script:

FILE=~/.screenoff
if [ -f $FILE ]; then
    xinput set-prop 8 "Device Enabled" 1
    xinput set-prop 6 "Device Enabled" 1
    xinput set-prop 9 "Device Enabled" 1
    xset dpms force on
    rm ~/.screenoff
else
    xinput set-prop 8 "Device Enabled" 0
    xinput set-prop 6 "Device Enabled" 0
    xinput set-prop 9 "Device Enabled" 0
    xset dpms force off
    touch ~/.screenoff
fi

#You cannot use this to turn the screen back on if i3lock or similar is running, as they would grab the keys

